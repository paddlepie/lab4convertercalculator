package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ExchangeActivity extends Activity implements OnClickListener {
	
	float exchangeRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_exchange);
		
		Button b1 = (Button)findViewById(R.id.btnConvertMoney);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSetting);
		b2.setOnClickListener(this);
		
		TextView tvRate = (TextView)findViewById(R.id.tvRate);
		exchangeRate = Float.parseFloat(tvRate.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.exchange, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.btnConvertMoney) {
			EditText etUSD = (EditText)findViewById(R.id.etUSD);
			TextView tvTHB = (TextView)findViewById(R.id.tvTHB);
			String res = "";
			try {
				float usd = Float.parseFloat(etUSD.getText().toString());
				float thb = usd * exchangeRate;
				res = String.format(Locale.getDefault(), "%.2f", thb);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			tvTHB.setText(res);
		}
		else if (id == R.id.btnSetting) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("exchangeRate", exchangeRate);
			startActivityForResult(i, 9999);
			// 9999 = a request code, it is a unique integer value for internally identifying the return value 
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			exchangeRate = data.getFloatExtra("exchangeRate", 32.0f);
			TextView tvRate = (TextView)findViewById(R.id.tvRate);
			tvRate.setText(String.format(Locale.getDefault(), "%.2f", exchangeRate));
		}
	}

	
}
